# RDS subnet group
resource "aws_db_subnet_group" "rds_subnet_g" {
  name       = "rds_subnet_g"
  subnet_ids = [aws_subnet.private_us_east_1a.id,aws_subnet.private_us_east_1b.id]
  tags = { 
     Name = "rds_subnet_g"
  }
}

# database security group
resource "aws_security_group" "database_sg" {
  name        = "database_sg"
  description = "allow inbound traffic to db"
  vpc_id      = aws_vpc.main.id

  ingress {
     from_port   = 5432
     to_port     = 5432
     protocol    = "tcp"
	 cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
     from_port   = 32768
     to_port     = 65535
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
  }
  
  tags = {
     Name = "database_sg"
  }
}

# database username variable
variable "db_username" {
  description = "database admin username"
  type        = string
  sensitive   = true
}


# database password variable
variable "db_password" {
  description = "database admin password"
  type        = string
  sensitive   = true
}

resource "aws_db_instance" "my-db" {
  identifier             = "my-db"
  instance_class         = "db.t2.micro"
  allocated_storage      = 5
  engine                 = "postgres"
  engine_version         = "12.12"
  skip_final_snapshot    = true
  publicly_accessible    = true
  db_subnet_group_name   = aws_db_subnet_group.rds_subnet_g.id
  vpc_security_group_ids = [aws_security_group.database_sg.id]
  multi_az               = false
  db_name                = "postgres"
  username               = var.db_username
  password               = var.db_password
}