data "aws_db_instance" "my-db" {
  db_instance_identifier = "my-db"
}



resource "helm_release" "flask-frontend" {
  name       = "flask-frontend"
  chart      = "./charts/flask-frontend"
}

resource "helm_release" "flask-backend" {
  name       = "flask-backend"
  chart      = "./charts/flask-backend"
  
  set {
  name  = "database.host"
  value = data.aws_db_instance.my-db.address
  }
  set {
    name  = "database.name"
    value = "postgres"
  }      
}