# Terraform 3tier

## Description: three-tier architecture with terraform, k8s and helm
************************************************************
 - custom VPC
 - 2 public subnets in different AZs for high availability
 - 2 private subnets in different AZs
 - EKS K8s cluster (scalable, on-demand)
 - RDS postgres instance
 - deploy application with helm_release from local chart files
*************************************************************

# Deploy:
`terraform apply -var-file="secrets.tfvars"`